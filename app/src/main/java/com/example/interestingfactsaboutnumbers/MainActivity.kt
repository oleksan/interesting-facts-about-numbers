package com.example.interestingfactsaboutnumbers

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.rememberNavController
import com.example.interestingfactsaboutnumbers.navigation.FactsNavHost
import com.example.interestingfactsaboutnumbers.ui.theme.InterestingFactsAboutNumbersTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            InterestingFactsAboutNumbersTheme {
                val navController = rememberNavController()
                val mViewModel = hiltViewModel<MainViewModel>()
                FactsNavHost(navController = navController, mViewModel = mViewModel)
            }
        }
    }
}