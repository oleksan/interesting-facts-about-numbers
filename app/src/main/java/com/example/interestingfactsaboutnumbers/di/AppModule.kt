package com.example.interestingfactsaboutnumbers.di

import android.content.Context
import androidx.room.Room
import com.example.interestingfactsaboutnumbers.data.api.ApiService
import com.example.interestingfactsaboutnumbers.data.db.FactDao
import com.example.interestingfactsaboutnumbers.data.db.FactsDatabase
import com.example.interestingfactsaboutnumbers.utils.Constans.Companion.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideFactDatabase(@ApplicationContext context: Context) =
        Room.databaseBuilder(
            context,
            FactsDatabase::class.java,
            "facts_database"
        ).build()

    @Provides
    fun baseUrl() = BASE_URL

    @Provides
    @Singleton
    fun provideRetrofit(baseUrl: String): ApiService =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
            .create(ApiService::class.java)


    @Provides
    fun provideFactDao(appDatabase: FactsDatabase): FactDao {
        return appDatabase.getDao()
    }
}