package com.example.interestingfactsaboutnumbers

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.interestingfactsaboutnumbers.data.api.ApiRepository
import com.example.interestingfactsaboutnumbers.data.api.RoomRepository
import com.example.interestingfactsaboutnumbers.models.Fact
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: RoomRepository,
    private val apiRepository: ApiRepository
) : ViewModel() {

    private val _randomFact = MutableLiveData<String>()
    val randomFact: LiveData<String>
        get() = _randomFact

    private val _fact = MutableLiveData<String>()
    val fact: LiveData<String>
        get() = _fact

    fun readAllFacts() = repository.getAllFactsFromBd()

    fun addFact(fact: Fact, onSuccess: () -> Unit) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.createNewFact(fact = fact) {
                viewModelScope.launch(Dispatchers.Main) {
                    onSuccess()
                }
            }
        }
    }

    fun getRandomFact() {
        viewModelScope.launch {
            apiRepository.getRandomFact().let {
                if (it.isSuccessful) {
                    _randomFact.postValue(it.body())
                } else {
                    Log.d("checkData", "Failed to load random fact: ${it.errorBody()}")
                }
            }
        }
    }

    fun getFact(number: String) {
        viewModelScope.launch {
            apiRepository.getFact(inputNumber = number).let {
                if (it.isSuccessful) {
                    _fact.postValue(it.body())
                } else {
                    Log.d("checkData", "Failed to load fact: ${it.errorBody()}")
                }
            }
        }
    }


}
