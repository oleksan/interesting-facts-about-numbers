package com.example.interestingfactsaboutnumbers.screens

import android.annotation.SuppressLint
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.text.isDigitsOnly
import androidx.navigation.NavHostController
import com.example.interestingfactsaboutnumbers.MainViewModel
import com.example.interestingfactsaboutnumbers.R
import com.example.interestingfactsaboutnumbers.models.Fact
import com.example.interestingfactsaboutnumbers.navigation.NavRoute
import com.example.interestingfactsaboutnumbers.utils.isNetworkAvailable
import kotlinx.coroutines.*


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun StartScreen(navController: NavHostController, viewModel: MainViewModel) {

    var numberFromTF by remember { mutableStateOf("") }
    var isButtonEnabled by remember { mutableStateOf(false) }

    val facts = viewModel.readAllFacts().observeAsState(listOf()).value

    val coroutineScope = rememberCoroutineScope()
    val context = LocalContext.current


    Scaffold(
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 30.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {
            OutlinedTextField(
                modifier = Modifier.width(200.dp),
                value = numberFromTF,
                onValueChange = {
                    numberFromTF = it
                    isButtonEnabled = numberFromTF.isNotEmpty() && numberFromTF.isDigitsOnly()
                },
                label = { Text(text = "Enter number") },
                isError = numberFromTF.isEmpty(),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
            )
            Button(
                onClick = {
                    if (isNetworkAvailable(context)) {
                        coroutineScope.launch(Dispatchers.IO) {
                            viewModel.getFact(numberFromTF)
                            delay(500)
                            val factFromInternet = viewModel.fact.value
                            if (factFromInternet != "" && factFromInternet != null) {
                                viewModel.addFact(
                                    fact = Fact(
                                        description = factFromInternet
                                    )
                                ) {
                                }
                            }
                        }
                    } else {
                        Toast.makeText(
                            context,
                            "Internet is not available",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                },
                modifier = Modifier
                    .width(200.dp)
                    .padding(vertical = 10.dp),
                enabled = isButtonEnabled
            ) {
                Text(text = "Get Fact")
            }

            Button(
                onClick = {
                    if (isNetworkAvailable(context)) {
                        coroutineScope.launch(Dispatchers.IO) {
                            viewModel.getRandomFact()
                            delay(500)
                            val factFromInternet = viewModel.randomFact.value
                            if (factFromInternet != "" && factFromInternet != null) {
                                viewModel.addFact(
                                    fact = Fact(
                                        description = factFromInternet
                                    )
                                ) {
                                }
                            }
                        }
                    } else {
                        Toast.makeText(
                            context,
                            "Internet is not available",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                },
                modifier = Modifier
                    .width(200.dp)
            ) {
                Text(text = "Get Random Fact")
            }
            Column(
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    modifier = Modifier
                        .padding(top = 10.dp)
                        .align(Alignment.CenterHorizontally),
                    text = "History:",
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold,
                    fontFamily = FontFamily.Serif
                )
            }
            LazyColumn(
                modifier = Modifier
                    .padding(top = 5.dp)
                    .background(colorResource(R.color.light_light_grey)),
                contentPadding = PaddingValues(all = 12.dp),
                verticalArrangement = Arrangement.spacedBy(8.dp),
            ) {
                items(facts) { fact ->
                    FactItem(fact = fact, navController = navController)
                }
            }
        }
    }
}

@Composable
fun FactItem(fact: Fact, navController: NavHostController) {
    val factId = fact.id
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 1.dp, horizontal = 4.dp)
            .clickable {
                navController.navigate(NavRoute.Description.route + "/${factId}")
            },
        elevation = 4.dp
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(vertical = 5.dp),
            horizontalArrangement = Arrangement.Start
        ) {
            Text(
                text = fact.description.toString(),
                fontSize = 18.sp,
                fontFamily = FontFamily.Serif,
                maxLines = 1,
                modifier = Modifier.padding(start = 8.dp)
            )
        }

    }
}
