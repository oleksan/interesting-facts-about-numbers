package com.example.interestingfactsaboutnumbers.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.interestingfactsaboutnumbers.MainViewModel
import com.example.interestingfactsaboutnumbers.R
import com.example.interestingfactsaboutnumbers.models.Fact

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun DescriptionScreen(navController: NavController, viewModel: MainViewModel, factId: String?) {

    val facts: List<Fact> = viewModel.readAllFacts().observeAsState(listOf()).value
    val fact = facts.firstOrNull { it.id == factId?.toInt() } ?: Fact()

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        backgroundColor = colorResource(R.color.light_light_grey)
    ) {
        Button(
            onClick = { navController.popBackStack() },
            modifier = Modifier.padding(start = 5.dp)
        ) {
            Row {
                Icon(
                    modifier = Modifier.size(24.dp),
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = ""
                )
                Text(
                    text = "Back",
                    fontSize = 16.sp,
                    modifier = Modifier.padding(start = 5.dp)
                )
            }
        }
        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = fact.description.toString(),
                fontSize = 18.sp,
                fontWeight = FontWeight.Light,
                fontFamily = FontFamily.Serif,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .padding(top = 32.dp, start = 32.dp, end = 32.dp)

            )

        }
    }
}


