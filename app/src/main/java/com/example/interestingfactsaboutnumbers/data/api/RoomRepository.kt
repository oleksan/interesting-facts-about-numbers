package com.example.interestingfactsaboutnumbers.data.api

import com.example.interestingfactsaboutnumbers.data.db.FactDao
import com.example.interestingfactsaboutnumbers.models.Fact
import javax.inject.Inject

class RoomRepository @Inject constructor(private val factDao: FactDao) {

    suspend fun createNewFact(fact: Fact, onSuccess: () -> Unit) {
        factDao.addFact(fact = fact)
        onSuccess()
    }

    fun getAllFactsFromBd() = factDao.getAllFacts()
}