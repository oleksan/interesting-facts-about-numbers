package com.example.interestingfactsaboutnumbers.data.api

import javax.inject.Inject

class ApiRepository @Inject constructor(private val apiService: ApiService){

    suspend fun getRandomFact() = apiService.getRandomFact()

    suspend fun getFact(inputNumber: String) = apiService.getFact(inputNumber)
}