package com.example.interestingfactsaboutnumbers.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.interestingfactsaboutnumbers.models.Fact

@Database(entities = [Fact::class], version = 1, exportSchema = true)
abstract class FactsDatabase : RoomDatabase() {

    abstract fun getDao(): FactDao

}
