package com.example.interestingfactsaboutnumbers.data.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("random/math")
    suspend fun getRandomFact(): Response<String>


    @GET("{number}")
    suspend fun getFact(@Path(value = "number") inputNumber: String): Response<String>
}