package com.example.interestingfactsaboutnumbers.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.interestingfactsaboutnumbers.models.Fact

@Dao
interface FactDao {

    @Query("SELECT * FROM facts_table ORDER BY id DESC")
    fun getAllFacts(): LiveData<List<Fact>>

    @Insert
    suspend fun addFact(fact: Fact)
}