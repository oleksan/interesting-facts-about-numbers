package com.example.interestingfactsaboutnumbers

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class InterestingFactsAboutNumbersApp: Application()
