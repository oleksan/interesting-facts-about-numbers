package com.example.interestingfactsaboutnumbers.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.interestingfactsaboutnumbers.MainViewModel
import com.example.interestingfactsaboutnumbers.screens.DescriptionScreen
import com.example.interestingfactsaboutnumbers.screens.StartScreen

sealed class NavRoute(val route: String) {
    object Start : NavRoute("start_screen")
    object Description : NavRoute("description_screen")
}

@Composable
fun FactsNavHost(navController: NavHostController, mViewModel: MainViewModel) {

    NavHost(navController = navController, startDestination = NavRoute.Start.route) {
        composable(NavRoute.Start.route) {
            StartScreen(
                navController = navController,
                viewModel = mViewModel
            )
        }
        composable(NavRoute.Description.route + "/{${"Id"}}") { backStackEntry ->
            DescriptionScreen(
                navController = navController,
                viewModel = mViewModel,
                factId = backStackEntry.arguments?.getString("Id")
            )
        }
    }
}
